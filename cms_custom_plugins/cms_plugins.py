from cms.plugin_base import CMSPluginBase
from django.contrib import admin
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _

from .models import Slider, SliderItem, PageLayout
from .models import PageCol, RoundedBorderContainer
from .models import SocialAds, ClassDisplay

class SliderPlugin(CMSPluginBase):
	name = _('Slider Container')
	allow_children = True
	model = Slider
	render_template = 'plugins/slider/slider.html'
	cache = False
	child_classes = ['SliderItemPlugin']

	def render(self, context, instance, placeholder):
		context = super(SliderPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(SliderPlugin)

class SliderItemPlugin(CMSPluginBase):
	model = SliderItem
	name = _('Slider Item')
	require_parent = True
	cache = False
	parent_classes = ['SliderPlugin']
	render_template = 'plugins/slider/slide.item.html'

	def render(self, context, instance, placeholder):
		context = super(SliderItemPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(SliderItemPlugin)

class PageLayoutPlugin(CMSPluginBase):
	model = PageLayout
	name = _('Page Layout')
	cache = False
	allow_children = True
	render_template = 'plugins/layout/page.layout.html'

	def render(self, context, instance, placeholder):
		context = super(PageLayoutPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(PageLayoutPlugin)

class PageColPlugin(CMSPluginBase):
	model = PageCol
	name = _('Page Column')
	cache = False
	require_parent = True
	allow_children = True
	parent_classes = ['PageLayoutPlugin']
	render_template = 'plugins/layout/col.layout.html'

	def render(self, context, instance, placeholder):
		context = super(PageColPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(PageColPlugin)

class RoundedBorderContainerPlugin(CMSPluginBase):
	model = RoundedBorderContainer
	name = _('Rounded Border Container')
	cache = False
	require_parent = True
	allow_children = True
	render_template = 'plugins/container/rounded.border.container.html'

	def render(self, context, instance, placeholder):
		context = super(RoundedBorderContainerPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(RoundedBorderContainerPlugin)

class SocialAdsPlugin(CMSPluginBase):
	model = SocialAds
	name = _('Social Ads')
	cache = False
	require_parent = True
	allow_children = True
	render_template = 'plugins/socials/social.ads.html'

	def render(self, context, instance, placeholder):
		context = super(SocialAdsPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(SocialAdsPlugin)

class ClassDisplayPlugin(CMSPluginBase):
	model = ClassDisplay
	name = _('Class Display')
	cache = False
	require_parent = True
	allow_children = True
	render_template = 'plugins/panels/panel.html'

	def render(self, context, instance, placeholder):
		context = super(ClassDisplayPlugin, self).render(context, instance, placeholder)
		return context
plugin_pool.register_plugin(ClassDisplayPlugin)