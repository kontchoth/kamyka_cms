# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0002_pagelayout'),
    ]

    operations = [
        migrations.CreateModel(
            name='PageCol',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_pagecol', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('max_width', models.FloatField(default=50.0, verbose_name='Max Width')),
                ('unit', models.CharField(default=b'%', max_length=10, verbose_name='Width unit', choices=[(b'%', b'%'), (b'px', b'px')])),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
