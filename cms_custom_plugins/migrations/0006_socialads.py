# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0005_roundedbordercontainer'),
    ]

    operations = [
        migrations.CreateModel(
            name='SocialAds',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_socialads', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(max_length=200, null=True, verbose_name='Ads Name', blank=True)),
                ('img', models.ImageField(upload_to=b'ads/')),
                ('widget_id', models.CharField(max_length=300, null=True, verbose_name='Widget ID', blank=True)),
                ('url', models.CharField(max_length=300, null=True, verbose_name='Ads Url', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
