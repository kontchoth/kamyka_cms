# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0003_pagecol'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pagecol',
            name='max_width',
        ),
        migrations.AddField(
            model_name='pagecol',
            name='width',
            field=models.FloatField(default=50.0, verbose_name='Width'),
        ),
    ]
