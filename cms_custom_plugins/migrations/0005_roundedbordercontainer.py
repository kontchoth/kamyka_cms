# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0016_auto_20160608_1535'),
        ('cms_custom_plugins', '0004_auto_20170410_1316'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoundedBorderContainer',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, related_name='cms_custom_plugins_roundedbordercontainer', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('border_color', colorfield.fields.ColorField(default=b'#6E0012', max_length=10, verbose_name='Border Color')),
                ('border_thickness', models.IntegerField(default=1, verbose_name='Border thickness')),
                ('border_radius', models.IntegerField(default=20, verbose_name='Border raduis')),
                ('content', djangocms_text_ckeditor.fields.HTMLField(null=True, verbose_name='Container Content', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
