# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0007_auto_20170410_1407'),
    ]

    operations = [
        migrations.AlterField(
            model_name='socialads',
            name='img',
            field=models.ImageField(default=None, upload_to=b'ads/', blank=True),
        ),
    ]
