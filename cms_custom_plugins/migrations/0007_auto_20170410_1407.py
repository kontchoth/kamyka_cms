# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_custom_plugins', '0006_socialads'),
    ]

    operations = [
        migrations.AlterField(
            model_name='socialads',
            name='img',
            field=models.ImageField(null=True, upload_to=b'ads/', blank=True),
        ),
    ]
