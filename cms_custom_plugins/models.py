from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from .validators import validate_video_extension
from djangocms_text_ckeditor.fields import HTMLField
from colorfield.fields import ColorField

from django.db import models

UNIT_CHOICES = (('%', '%'), ('px','px'))

class Slider(CMSPlugin):
	slide_duration = models.IntegerField(_('Slide duration (ms)'), null=False, blank=False, default=3000)

	def __unicode__(self):
		return 'Slider {}ms'.format(self.slide_duration)

	def __str__(self):
		return 'Slider {}ms'.format(self.slide_duration)

class SliderItem(CMSPlugin):
	image = models.ImageField(upload_to='slider/images/', blank=False, null=False)
	title = models.CharField(null=True, blank=True, max_length=200)
	message = models.CharField(null=True, blank=True, max_length=200)


	def __inicode__(self):
		return 'Slide Item {}'.format(self.title)

	def __str__(self):
		return 'Slide Item {}'.format(self.title)

class PageLayout(CMSPlugin):
	max_width = models.FloatField(_('Max Width'), default=50.0)
	unit = models.CharField(_('Width unit'), max_length=10, blank=False, default='%', choices=UNIT_CHOICES)
	num_cols = models.IntegerField(_('Number of columns'), blank=False, null=False, default=1)

	def __unicode__(self):
		return 'Page Layout {} column(s)'.format(self.num_cols)

	def __str__(self):
		return 'Page Layout {} column(s)'.format(self.num_cols)

class PageCol(CMSPlugin):
	width = models.FloatField(_('Width'), default=50.0)
	unit = models.CharField(_('Width unit'), max_length=10, blank=False, default='%', choices=UNIT_CHOICES)

	def __unicode__(self):
		return 'Page Layout {} {}'.format(self.width, self.unit)

	def __str__(self):
		return 'Page Layout {} {}'.format(self.width, self.unit)

class RoundedBorderContainer(CMSPlugin):
	border_color = ColorField(_('Border Color'), blank=False, null=False, default='#6E0012')
	border_thickness = models.IntegerField(_('Border thickness'), null=False, blank=False, default=1)
	border_radius = models.IntegerField(_('Border raduis'), null=False, blank=False, default=20)
	content = HTMLField(_('Container Content'), blank=True, null=True)

	def __unicode__(self):
		return 'Rounded Border Container'

	def __str__(self):
		return 'Rounded Border Container'

class SocialAds(CMSPlugin):
	name = models.CharField(_('Ads Name'), max_length=200, blank=True, null=True)
	img = models.ImageField(upload_to='ads/', blank=True, default=None)
	widget_id = models.CharField(_('Widget ID'), max_length=300, blank=True, null=True)
	url = models.CharField(_('Ads Url'), max_length=300, blank=True, null=True)

	def __unicode__(self):
		return 'Social Ads {}'.format(self.name)

	def __str__(self):
		return 'Social Ads {}'.format(self.name)

class ClassDisplay(CMSPlugin):
	title = models.CharField(_('Panel Title'), max_length=200)
	img = models.ImageField(_('Panel image'), upload_to='classes/', blank=False, null=False)
	description = models.TextField()

	def __unicode__(self):
		return 'Class display {}'.format(self.title)

	def __str__(self):
		return 'Class display {}'.format(self.title)